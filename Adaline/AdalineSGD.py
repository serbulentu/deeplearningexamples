'''
Code from https://sebastianraschka.com/Articles/2015_singlelayer_neurons.html#adaptive-linear-neurons-and-the-delta-rule
Author: Sebastian Raschka
Adapted for OR problem by: Serbulent Unsal
'''

import numpy as np

class AdalineSGD(object):

    def __init__(self, eta=0.01, epochs=50):
        self.eta = eta
        self.epochs = epochs

    def train(self, X, y, reinitialize_weights=True):

        if reinitialize_weights:
            self.w_ = np.zeros(1 + X.shape[1])
        self.cost_ = []

        for i in range(self.epochs):
            for xi, target in zip(X, y):
                output = self.net_input(xi)
                error = (target - output)
                if len(error) > 1:
                    self.w_[1:] += self.eta * xi.dot(error)
                else:
                    self.w_[1:] += self.eta * xi * error
                self.w_[0] += self.eta * error

            cost = ((y - self.activation(X))**2).sum() / 2.0
            print("Cost: " + str(cost))
            self.cost_.append(cost)
        return self

    def net_input(self, X):
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
        return self.net_input(X)

    def predict(self, X):
        return np.where(self.activation(X) >= 0.0, 1, -1)

ada = AdalineSGD(epochs=10, eta=0.1)

# F I R S T   P R O B L E M - L O G I C A L   O R   L I N E A R
# input dataset representing the logical OR operator (including constant BIAS input of 1)
INPUTS = np.array([[-1,-1,1],
                   [-1,1,1],
                   [1,-1,1],
                   [1,1,1] ])
# output dataset - Only output a -1 if both inputs are -1
OUTPUTS = np.array([[-1,1,1,1]]).T

ada.train(INPUTS, OUTPUTS)

print(ada.predict([-1,-1,1]) == -1)

print(ada.predict([1,-1,1]) == 1)

print(ada.predict([-1,1,1]) == 1 )

print(ada.predict([1,1,1]) == 1 )

