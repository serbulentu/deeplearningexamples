'''Code From https://medium.com/@thomascountz/19-line-line-by-line-python-perceptron-b6f113b161f32
by Thomas Countz
'''

import numpy as np

class Perceptron(object):

    def __init__(self, no_of_inputs, threshold=10, learning_rate=0.01):
        self.threshold = threshold
        self.learning_rate = learning_rate
        self.weights = np.zeros(no_of_inputs + 1)

    def predict(self, inputs):
        summation = np.dot(inputs, self.weights[1:]) + self.weights[0]
        if summation > 0:
            activation = 1
        else:
            activation = 0

        return activation

    def train(self, training_inputs, labels):
        for i in range(self.threshold):
            print("\n\nStep:" + str(i))
            for inputs, label in zip(training_inputs, labels):
                prediction = self.predict(inputs)
                self.weights[1:] += self.learning_rate * (label - prediction) * inputs
                self.weights[0] += self.learning_rate * (label - prediction)

                print("Error: ")
                print((label - prediction))

                print("Weights: ")
                print(self.weights)





#=> 0