'''Code From https://medium.com/@thomascountz/19-line-line-by-line-python-perceptron-b6f113b161f32
by Thomas Countz
'''

import numpy as np
from Perceptron import Perceptron

training_inputs = []
training_inputs.append(np.array([1, 1]))
training_inputs.append(np.array([1, 0]))
training_inputs.append(np.array([0, 1]))
training_inputs.append(np.array([0, 0]))

labels = np.array([0, 1, 1, 1])

perceptron = Perceptron(2)
perceptron.train(training_inputs, labels)

inputs = np.array([1, 1])
print(perceptron.predict(inputs))
#=> 1

inputs = np.array([0, 1])
print(perceptron.predict(inputs))
#=> 0